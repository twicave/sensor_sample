/*
 * 传感器采样的核心业务逻辑部分
 * bi.h
 * created by fengxh @May17,2024
 * ----
 * May18,2024 添加除SampleThenSentout()函数之外的其他部分。开始使用gitcode。
 */
#ifndef BI_H
#define BI_H

#include <stdbool.h>

typedef struct _SAMPLE_PARAMS
{
    char *ip;
    int saps;
    int chcnt;
    int *chlist;
    int ptps;
    int vol_standard;
    char *memo;
}SAMPLE_PARAMS, *LPSAMPLE_PARAMS;

void InitSampleParams(LPSAMPLE_PARAMS param);
bool IsSampleParamValid(LPSAMPLE_PARAMS param);
bool DoSample(LPSAMPLE_PARAMS param, void *tgtBuf, int *len);
int SendSampleRawData2Stdout(LPSAMPLE_PARAMS param, void *tgtBuf, int *len);
#endif